---
type: slide
slideOptions:
  transition: slide
---

# Parallel programming in Go and CUDA

---

## Outline

- Comparison of hardware
- Programming Models
- Conclusion
- Demo

---

## Comparison of hardware

---

### CPU

- big cores (CISC)
- few powerful cores
- complicated instructions in few cycles (SIMD)
- fast cores
- slow interface to main memory
- big cache

Note:
- space restrictions
- commercial reasons

---

### GPU

- many many small cores (RISC)
- compute same instruction many times at the same time (SIMT)
- GPC -> SM -> Warp -> CUDA Core
- Warp:
    - only one scheduler
    - only one instruction at a time
- very fast memory
- small cache

Note:
- Pascal
- not "real" cores
- Graphics Processing Cluster
- Streaming Multiprocessor

- fetch one instruction -> apply it to many data
- threads have to agree on instruction
- other threads have to wait

|                  | CPU             | GPU      |
| ---------------- | --------------- | -------- |
| cores            | 8               | 3584/112 |
| memory bandwidth | up to 20 GB/s   | 484 GB/s |
| memory bus width | 64 bit          | 352 bit  |

----

![GP100 Block Diagram](https://codeberg.org/crapStone/CPPGC/raw/branch/main/doc/assets/gp100_block_diagram-1.png)

----

![GP100 Streaming Multiprocessor](https://codeberg.org/crapStone/CPPGC/raw/branch/main/doc/assets/gp100_SM_diagram.png =800x)

---

## Programming Models

----

| CPU       | GPU       |
| --------- | --------- |
| SISD/SIMD | SIMT/SIMD |
| data stream | block of data |
| control over scheduler | no control over scheduler |

----

##### Goroutines

```go
func main() {
  go myGoroutine()
}

func myGoroutine() {
  fmt.Println("Hello World")
}
```

Note:
- scheduler: M to N
- cooperative work-stealing vs. preemptive
- small -> less memory/metadata
- (NUMA)

----

##### Channels

```go
func main() {
  c := make(chan string)

  go generator(c)

  fmt.Println(<-c)
}

func generator(c chan string) {
  time.Sleep(1 * time.Second)

  c <- "Hello World"
}
```

Note:
- communication and synchronization

---

### GPU

----

###### Example Go function

```go
func foo(a, b, c [32][32]int) {
  // for every row
  for x := 0; x < 32; x++ {
    // for every column
    for y := 0; y < 32; y++ {
      // this block can be parallelized
      tmp := 0
      for k := 0; k < 32; k++ {
        tmp += a[x][k] + b[k][y]
      }

      c[x][y] = tmp
    }
  }
}
```

----

###### CUDA kernel

```cpp
__global__ void matrix_mul(
  const int *a, const int *b, int *c, int N
){
  int row = blockIdx.y * blockDim.y + threadIdx.y;
  int col = blockIdx.x * blockDim.x + threadIdx.x;

  int result = 0;
  for (int k = 0; k < N; k++) {
      result += a[row * N + k] * b[k * N + col];
  }

  c[row * N + col] = result;
}
```

```cu
matrix_mul<<<grid, threads>>>(a, b, c, 1024);
```

Note:
- loops -> how many times kernel-execution
- grid * threads = N = number of threads
- grid -> group of blocks (1,2,3-dim)
- block -> group of threads on same SM (1,2,3-dim)

----

###### Allocate data

```cpp
// size of matrix
size_t MATRIX_SIZE = N * N * sizeof(int);

// allocate host arrays
std::vector<int> host_a(N * N);
std::vector<int> host_b(N * N);
std::vector<int> host_c(N * N);

// allocate device memory
int *device_a, *device_b, *device_c;
cudaMalloc(&device_a, MATRIX_SIZE);
cudaMalloc(&device_b, MATRIX_SIZE);
cudaMalloc(&device_c, MATRIX_SIZE);
```

```cpp
cudaMallocManaged(&device_a, MATRIX_SIZE);
```

Note:
- host: computer program runs on
- device: GPU kernel runs on
- best practice: prefix `h` `d`
- managed memory: `cudaMallocManaged(&pointer, size)`
    - pointer can be accessed on both
    - not deterministic at runtime

----

###### Manual memory management

```cpp
cudaMemcpy(device_a, host_a.data(), MATRIX_SIZE,
  cudaMemcpyHostToDevice);
cudaMemcpy(device_b, host_b.data(), MATRIX_SIZE,
  cudaMemcpyHostToDevice);
```

```cpp
cudaMemcpy(host_c.data(), device_c, MATRIX_SIZE,
  cudaMemcpyDeviceToHost);
```

```cpp
cudaDeviceSynchronize();
```

```cpp
cudaFree(device_a);
cudaFree(device_b);
cudaFree(device_c);
```

----

###### Grid and Block size calculations

```cpp
int THREADS = 32;

int BLOCKS = N / THREADS;

dim3 threads(THREADS, THREADS);
dim3 grid(BLOCKS, BLOCKS);

matrix_mul<<<grid, threads>>>(device_a, device_b, device_c, N);
```

Note:
- 32: Warp size -> all threads should run at the same time
- exact division or padding with bounds checks in kernel
- two dimensional because matrices

---

## Conclusion

Note:
- Go:
    - easy to learn
    - simple concurrency principles
- CUDA:
    - powerful (computation)
    - hard to wrap mind around
    - many ways of optimization
    - at the mercy of NVIDIA
