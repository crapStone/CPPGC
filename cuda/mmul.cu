#include <cassert>
#include <chrono>
#include <iostream>
#include <vector>

#include "args.h"
#include "kernels.h"

using namespace std;
// time measurement
using namespace std::chrono;

// check result on the CPU
void verify_result(std::vector<int> &a, std::vector<int> &b, std::vector<int> &c, int N, const Args &args) {
  if (!args.quiet) {
    std::cout << "checking result of computation..." << std::endl;
  }

  // for every row
  for (int i = 0; i < N; i++) {
    // for every column
    for (int j = 0; j < N; j++) {
      // for every element in the row-column pair compute a value in output array
      int tmp = 0;
      for (int k = 0; k < N; k++) {
        tmp += a[i * N + k] * b[k * N + j];
      }

      // check GPU result against CPU
      assert(tmp == c[i * N + j]);
    }
  }

  std::cout << "SUCCESSFUL" << std::endl;
}

int main(int argc, char *argv[]) {
  Args args = parse_args(argc, argv);

  // matrix dimension
  int N = args.matrix_size;

  // size of matrix
  size_t MATRIX_SIZE = N * N * sizeof(int);

  if (!args.quiet) {
    std::cout << "size of one matrix: " << (N * N) << " elements, " << (MATRIX_SIZE >> 10) << " KiB" << std::endl;
  }

  // allocate host arrays
  std::vector<int> host_a(N * N);
  std::vector<int> host_b(N * N);
  std::vector<int> host_c(N * N);

  // initialize matrices
  for (int x = 0; x < N; x++) {
    for (int y = 0; y < N; y++) {
      host_a[x * N + y] = (x * y) % 100;
    }
  }

  // initialize b with the same values as a
  std::copy(std::begin(host_a), std::end(host_a), std::begin(host_b));

  // allocate device memory
  int *device_a, *device_b, *device_c;
  cudaMalloc(&device_a, MATRIX_SIZE);
  cudaMalloc(&device_b, MATRIX_SIZE);
  cudaMalloc(&device_c, MATRIX_SIZE);

  // copy arrays to the device
  cudaMemcpy(device_a, host_a.data(), MATRIX_SIZE, cudaMemcpyHostToDevice);
  cudaMemcpy(device_b, host_b.data(), MATRIX_SIZE, cudaMemcpyHostToDevice);

  // blocks per grid dimension (assumes THREADS divides N evenly)
  int BLOCKS = N / THREADS;

  // two dimensional block and grid dimensions
  dim3 threads(THREADS, THREADS);
  dim3 grid(BLOCKS, BLOCKS);

  auto start = chrono::high_resolution_clock::now();

  // launch compute kernel
  switch (args.method) {
  case Method::NAIVE:
    matrix_mul<<<grid, threads>>>(device_a, device_b, device_c, N);
    break;
  case Method::PREFETCH:
    matrix_mul_prefetch<<<grid, threads>>>(device_a, device_b, device_c, N);
    break;
  case Method::CACHE:
    matrix_mul_cached<<<grid, threads>>>(device_a, device_b, device_c, N);
    break;
  case Method::PREFETCH_CACHE:
    matrix_mul_prefetch_cached<<<grid, threads>>>(device_a, device_b, device_c, N);
    break;
  }

  // wait for device to finish
  cudaDeviceSynchronize();
  auto finish = chrono::high_resolution_clock::now();

  // copy result array back to the host
  cudaMemcpy(host_c.data(), device_c, MATRIX_SIZE, cudaMemcpyDeviceToHost);

  chrono::duration<double, std::milli> elapsed = finish - start;
  if (args.quiet) {
    std::cout << elapsed.count() << std::endl;
  } else {
    std::cout << "compute kernel execution time: " << elapsed.count() << "ms" << std::endl;
  }

  // check result
  if (args.test_result) {
    verify_result(host_a, host_b, host_c, N, args);
  }

  // free memory on device
  cudaFree(device_a);
  cudaFree(device_b);
  cudaFree(device_c);

  return 0;
}
