import unittest
import re
import subprocess


class TestArguments(unittest.TestCase):
    def test_no_args(self):
        out = subprocess.check_output("./mmul")
        self.assertEqual(out.strip(), b"usage: ./mmul [-m <matrix_size>] [-chpqt]")

    def test_matrix_size(self):
        out = subprocess.check_output("./mmul -m 2048".split(" "))

        self.assertEqual(
            out.split(b"\n")[0], b"size of one matrix: 4194304 elements, 16384 KiB"
        )

    def test_default_matrix_size(self):
        out = subprocess.check_output("./mmul -p".split(" "))

        self.assertEqual(
            out.split(b"\n")[0], b"size of one matrix: 1048576 elements, 4096 KiB"
        )


class TestKernelsCorrectness(unittest.TestCase):
    def test_naive(self):
        out = subprocess.check_output("./mmul -m 32 -t".split(" "))
        self.assertEqual(out.strip().split(b"\n")[-1], b"SUCCESSFUL")

    def test_prefetch(self):
        out = subprocess.check_output("./mmul -m 32 -t -p".split(" "))
        self.assertEqual(out.strip().split(b"\n")[-1], b"SUCCESSFUL")

    def test_cache(self):
        out = subprocess.check_output("./mmul -m 32 -t -c".split(" "))
        self.assertEqual(out.strip().split(b"\n")[-1], b"SUCCESSFUL")

    def test_prefetch_cache(self):
        out = subprocess.check_output("./mmul -m 32 -t -p -c".split(" "))
        self.assertEqual(out.strip().split(b"\n")[-1], b"SUCCESSFUL")


class TestKernelsSpeed(unittest.TestCase):
    def test_prefetch(self):
        out1 = subprocess.check_output("./mmul -m 1024 -q".split(" "))
        out2 = subprocess.check_output("./mmul -m 1024 -q -p".split(" "))
        self.assertLess(float(out2.strip()), float(out1.strip()))

    def test_cache(self):
        out1 = subprocess.check_output("./mmul -m 1024 -q".split(" "))
        out2 = subprocess.check_output("./mmul -m 1024 -q -c".split(" "))
        self.assertLess(float(out2.strip()), float(out1.strip()))

    def test_cache_prefetch(self):
        out1 = subprocess.check_output("./mmul -m 1024 -q -c".split(" "))
        out2 = subprocess.check_output("./mmul -m 1024 -q -p".split(" "))
        out3 = subprocess.check_output("./mmul -m 1024 -q -c -p".split(" "))
        self.assertLess(float(out3.strip()), float(out1.strip()))
        self.assertLess(float(out3.strip()), float(out2.strip()))


if __name__ == "__main__":
    unittest.main()
